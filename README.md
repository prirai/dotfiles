# dotfiles

![Sway Wallpaper](current/sway/config.d/wallp.jpg)

### Install
Simple script to install all the project folders to the config folders.

Select a date from which to restore configs, optionally ask to backup with a desired foldername.

### Backit
Simple script to backup all the required config files from the `.config` folder.

### Restore

_Restores previously saved backup_.

Asks the user to restore a previously saved backup and not necessarily the dated folders.
