uptime_formatted=$(uptime | cut -d ',' -f1 | sed -En "s/up/🔺/p")
linux_version=$(uname -r | cut -d '-' -f1)
battery_status=$(cat /sys/class/power_supply/BAT0/capacity)"%";

if [ $(cat /sys/class/power_supply/BAT0/status) = "Discharging" ];
then
    	BATSTATE="🔋";
else
    	BATSTATE="🔌";
fi

date_loc=$(date "+| 🗒 %d｡%m｡%y")
bright_stat=$(cat /sys/class/backlight/amdgpu_bl0/actual_brightness)"/"$(cat /sys/class/backlight/amdgpu_bl0/max_brightness)
brsym=$(cat /sys/class/backlight/amdgpu_bl0/actual_brightness)

if [ $brsym -gt 200 ]
then
    	brsym="🌝"
fi
if [ $brsym -le 200 ] && [ $brsym -gt 150 ]
then
    	brsym="🌖"
fi
if [ $brsym -le 150 ] && [ $brsym -gt 100 ]
then
    	brsym="🌗"
fi
if [ $brsym -le 100 ] && [ $brsym -gt 50 ]
then
    	brsym="🌘"
fi
if [ $brsym -le 50 ] && [ $brsym -ge 0 ]
then
    	brsym="🌚"
fi

lfvol=$(pactl list sinks | grep 'Volume: front-left' | cut -d '/' -f2 | cut -d "%" -f1)
rtvol=$(pactl list sinks | grep 'Volume: front-right' |	cut -d '/' -f2 | cut -d "%" -f1)

if [ $(lfvol)==$(rfvol)];
then
	finvol=$(lfvol);
else
	finvol=" L:"$(lfvol)" R: "$(rfvol);
fi


# Network connection 
nethost=$(iwconfig wlp2s0 | grep "ESSID" | cut -d ':' -f2)
if [$(nethost)=="off/any"];
then
	nethost="disconnected"
fi
echo \| $nethost \| "Vol:"$lfvol"%" \(🐧$linux_version\) $brsym $bright_stat [$BATSTATE $battery_status $batstate] $date_loc \|" 🌐"$uptime_formatted
